﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    private bool moveDown;
    private float _bottom;
    [SerializeField] private Controller m_Controller;
    
    // Start is called before the first frame update
    void MoveDown(float bottom)
    {
        moveDown = true;
        _bottom = bottom;
    }

    // Update is called once per frame
    void Update()
    {
        if (moveDown)
        {
            float coordY = transform.position.y;
            if (coordY >= _bottom)
            {
                StopMoving();
                return;
            }

            coordY += Time.deltaTime * 0.5f;
            transform.position = new Vector3(transform.position.x, coordY, transform.position.z);
        }
    }

    private void StopMoving()
    {
        moveDown = false;
    }
}
