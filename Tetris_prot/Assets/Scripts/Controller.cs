﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class Controller : MonoBehaviour
{
    private GameObject _leftBorder;
    private GameObject _rightBorder;
    
    private GameObject _currentBlock;
    private GameObject _nextBlock;
    private float _bottom;
    
    [SerializeField] private List<GameObject> m_Blocks;  
    
    
    private enum EGameState
    {
        Move,
        Reset,
    }
    
    private EGameState _currentGameState;
    
    // Start is called before the first frame update
    void Start()
    {
        _bottom = 0;
        GameObject border = new GameObject();
          //init borders
          for (int i = 0; i < 15; i++)
          {
              GameObject a = GameObject.CreatePrimitive(PrimitiveType.Cube);
              a.transform.position = new Vector3(0,i,0);
              a.transform.SetParent(border.transform);
          }

          _leftBorder = Instantiate(border, new Vector3(-0.1f, _bottom, 0),Quaternion.identity);
          _leftBorder.transform.localScale = new Vector3(0.1f,1,1);
          _rightBorder = Instantiate(border, new Vector3(15f, _bottom, 0),Quaternion.identity);
          _rightBorder.transform.localScale = new Vector3(0.1f,1,1);

          Destroy(border);
          
          

          //creating next element
          CreateNextBlock();
          
          
          _currentGameState = EGameState.Reset;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_currentGameState)
        {
            case EGameState.Move: 
                Move();
                break;
            
            case EGameState.Reset:
                Reset();
                _currentGameState = EGameState.Move;
                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        int currentY = (int) _currentBlock.transform.position.y;
        
        if (Input.GetKeyDown("a"))
        {
            if (_leftBorder.transform.position.x+1 < _currentBlock.transform.position.x)
            {
                _currentBlock.transform.position = new Vector3(_currentBlock.transform.position.x - 1f,
                    _currentBlock.transform.position.y, _currentBlock.transform.position.z);
            }
        }
        if (Input.GetKeyDown("d"))
        {
            if (_rightBorder.transform.position.x-1 > _currentBlock.transform.position.x)
            {
                _currentBlock.transform.position = new Vector3(_currentBlock.transform.position.x + 1f,
                    _currentBlock.transform.position.y, _currentBlock.transform.position.z);
            }
        }
        if (Input.GetKeyDown("w"))
        {
            if (_rightBorder.transform.position.x-1 > _currentBlock.transform.position.x)
            {
                _currentBlock.transform.position = new Vector3(_currentBlock.transform.position.x - 1f,
                    _currentBlock.transform.position.y, _currentBlock.transform.position.z);
            }
        }
       
    }

    private void Move()
    {   
            float coordY = _currentBlock.transform.position.y;
            if (coordY <= _bottom)
            {
                _currentGameState = EGameState.Reset;
                
                Destroy(_currentBlock);
                
                return;
            }

            coordY -= Time.deltaTime * 3f;
            _currentBlock.transform.position = new Vector3(_currentBlock.transform.position.x, coordY, _currentBlock.transform.position.z);

    }

    private void Reset()
    {
        _currentBlock = _nextBlock;
        _currentBlock.transform.position = new Vector3(7.5f,14f,0);
        CreateNextBlock();
    }

    private void CreateNextBlock()
    {
        GameObject block = m_Blocks[Random.Range(0, m_Blocks.Count-1)];
        _nextBlock = Instantiate(block, new Vector3(18f, 14f, 0),Quaternion.identity);
    }
}
